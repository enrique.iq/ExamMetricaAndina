# Examen Analista Programador .Net - Métrica Andina

### Solución del examen para el puesto de analista programador .Net 

Para la configuración de la aplicación y las pruebas respectivas, es necesario leer el documento de **Instrucciones.docx** 

## Acceso administrador

**Usuario:** jose@hotmail.com

**Contraseña:** Pa$w0rd

## Acceso Operador 1

**Usuario:** carlos@hotmail.com

**Contraseña:** Pa$w0rd

## Acceso Operador 2

**Usuario:** maria@hotmail.com

**Contraseña:** Pa$w0rd


## ScreenShot

**Inicio de la aplicación**

![alt tag](ScreenShot/1.png "Inicio de la aplicación")

**Logeo**


![alt tag](ScreenShot/2.png "Logeo")

**Ingreso a la aplicación**

![alt tag](ScreenShot/3.png "Ingreso a la aplicación")

**Listado de bancos**

![alt tag](ScreenShot/4.png "Listado de bancos")

**Listado de Sucursales**

![alt tag](ScreenShot/5.png "Listado de Sucursales")

**Listado de Orden de pago**

![alt tag](ScreenShot/6.png "Listado de Orden de pago")