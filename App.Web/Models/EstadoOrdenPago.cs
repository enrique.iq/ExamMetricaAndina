﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models
{
    public class EstadoOrdenPago : EntityBase
    {

        public EstadoOrdenPago()
        {
            OrdenPago = new HashSet<OrdenPago>();
        }
        [DisplayName("Código estado orden")]
        public int? EstadoOrdenPagoId { get; set; }
        [DisplayName("Nombre estado orden")]
        public string Nombre { get; set; }
        public virtual ICollection<OrdenPago> OrdenPago { get; set; }
    }
}
