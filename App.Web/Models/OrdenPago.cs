﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models
{
    public class OrdenPago : EntityBase
    {
        public int? OrdenPagoId { get; set; }
        public int? SucursalId { get; set; }
        [Required(ErrorMessage = "El campo es requerido")]
        public decimal? Monto { get; set; }
        public int? MonedaId { get; set; }
        [Required(ErrorMessage = "El campo es requerido")]
        public DateTime? FechaPago { get; set; }
        public int? EstadoOrdenPagoId { get; set; }
        public virtual EstadoOrdenPago EstadoOrdenPago { get; set; }
        public virtual Sucursal Sucursal { get; set; }
        public virtual Moneda Moneda { get; set; }
    }
}
