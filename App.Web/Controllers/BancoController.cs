﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using App.Web.Models;
using System.Net.Http;
using Newtonsoft.Json;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Controllers
{
    public class BancoController : Controller
    {
        string url = "http://localhost:54966/api/banco";
        HttpClient client = new HttpClient();
        public BancoController()
        {
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("applicacion/json"));
        }
        public async Task<ActionResult> Index()
        {
            List<Banco> lst = new List<Banco>();
            try
            {
                HttpResponseMessage resposne = await client.GetAsync(url);
                if (resposne.IsSuccessStatusCode)
                {
                    var obj = resposne.Content.ReadAsStringAsync().Result;
                    lst = JsonConvert.DeserializeObject<List<Banco>>(obj);
                }
            }
            catch (Exception)
            {

                return View();
            }


            return View(lst);
        }
        public async Task<ActionResult> Details(int id)
        {
            var result = new Banco();
            try
            {
                HttpResponseMessage resposne = await client.GetAsync(url + "/" + id);
                if (resposne.IsSuccessStatusCode)
                {
                    var obj = resposne.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<Banco>(obj);
                }
                return View(result);
            }
            catch
            {
                return RedirectToAction("Error");
            }

        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Create([Bind(include: "BancoId,Nombre,Direccion")]Banco banco)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync(url, banco);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return RedirectToAction("Error");
            }

            return RedirectToAction("Error");
        }

        public async Task<ActionResult> Edit(int id)
        {

            var result = new Banco();
            try
            {
                HttpResponseMessage resposne = await client.GetAsync(url + "/" + id);
                if (resposne.IsSuccessStatusCode)
                {
                    var obj = resposne.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<Banco>(obj);
                }
                return View(result);
            }
            catch
            {
                return RedirectToAction("Error");
            }
        }

        //The PUT Method
        [HttpPost]
        public async Task<ActionResult> Edit(int id, Banco banco)
        {
            try
            {
                HttpResponseMessage responseMessage = await client.PutAsJsonAsync(url + "/" + id, banco);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Error");
            }
            catch
            {
                return RedirectToAction("Error");
            }

        }

        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                HttpResponseMessage responseMessage = await client.DeleteAsync(url + "/" + id);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Error");
            }
            catch (Exception)
            {

                return RedirectToAction("Error");
            }

        }

        ////The DELETE method
        //[HttpPost]
        //public async Task<ActionResult> Delete(int id, Banco banco)
        //{
        //    try
        //    {
        //        HttpResponseMessage responseMessage = await client.DeleteAsync(url + "/" + id);
        //        if (responseMessage.IsSuccessStatusCode)
        //        {
        //            return RedirectToAction("Index");
        //        }
        //        return RedirectToAction("Error");
        //    }
        //    catch (Exception)
        //    {

        //        return RedirectToAction("Error");
        //    }
            
        //}

    }
}
