﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using App.Web.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace App.Web.Controllers
{
    public class SucursalController : Controller
    {
        string url = "http://localhost:54966/api/";
        HttpClient client = new HttpClient();
        public SucursalController()
        {
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("applicacion/json"));
        }
        public async Task<ActionResult> Index()
        {
            List<Sucursal> lst = new List<Sucursal>();
            try
            {
                HttpResponseMessage resposne = await client.GetAsync(url + "sucursal");
                if (resposne.IsSuccessStatusCode)
                {
                    var obj = resposne.Content.ReadAsStringAsync().Result;
                    lst = JsonConvert.DeserializeObject<List<Sucursal>>(obj);
                }
            }
            catch (Exception)
            {

                return View();
            }


            return View(lst);
        }
        public async Task<ActionResult> Details(int id)
        {
            var result = new Sucursal();
            try
            {
                HttpResponseMessage resposne = await client.GetAsync(url + "sucursal/" + id);
                if (resposne.IsSuccessStatusCode)
                {
                    var obj = resposne.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<Sucursal>(obj);
                }
                return View(result);
            }
            catch
            {
                return RedirectToAction("Error");
            }

        }
        public async Task<ActionResult> Create()
        {
            List<Banco> lst = new List<Banco>();
            try
            {
                HttpResponseMessage resposne = await client.GetAsync(url + "banco/");
                if (resposne.IsSuccessStatusCode)
                {
                    var obj = resposne.Content.ReadAsStringAsync().Result;
                    lst = JsonConvert.DeserializeObject<List<Banco>>(obj);
                    var items = lst.Select(x => new SelectListItem() { Text = x.Nombre, Value = x.BancoId.ToString() }).ToList();
                    ViewBag.lstBanco = items;
                }
            }
            catch (Exception)
            {

                return View();
            }

            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Create([Bind(include: "BancoId,SucursalId,Nombre,Direccion")]Sucursal obj)
        {
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync(url + "sucursal/", obj);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return RedirectToAction("Error");
            }

            return RedirectToAction("Error");
        }

        public async Task<ActionResult> Edit(int id)
        {

            var result = new Sucursal();
            try
            {
                List<Banco> lst = new List<Banco>();
                HttpResponseMessage resposneBanco = await client.GetAsync(url + "banco/");
                if (resposneBanco.IsSuccessStatusCode)
                {
                    var obj = resposneBanco.Content.ReadAsStringAsync().Result;
                    lst = JsonConvert.DeserializeObject<List<Banco>>(obj);
                    var items = lst.Select(x => new SelectListItem() { Text = x.Nombre, Value = x.BancoId.ToString() }).ToList();
                    ViewBag.lstBanco = items;
                }


                HttpResponseMessage resposne = await client.GetAsync(url + "sucursal/" + id);
                if (resposne.IsSuccessStatusCode)
                {
                    var obj = resposne.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<Sucursal>(obj);
                }
                return View(result);
            }
            catch
            {
                return RedirectToAction("Error");
            }
        }

        //The PUT Method
        [HttpPost]
        public async Task<ActionResult> Edit(int id, Sucursal obj)
        {
            try
            {
                HttpResponseMessage responseMessage = await client.PutAsJsonAsync(url + "sucursal/" + id, obj);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Error");
            }
            catch
            {
                return RedirectToAction("Error");
            }

        }

        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                HttpResponseMessage responseMessage = await client.DeleteAsync(url + "sucursal/" + id);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Error");
            }
            catch (Exception)
            {

                return RedirectToAction("Error");
            }

        }


    }
}
