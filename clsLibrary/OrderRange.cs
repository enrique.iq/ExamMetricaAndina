﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clsLibrary
{
    public class OrderRange
    {
        public int[][] Build(int[] numeros)
        {
            int tamanio = numeros.Count();
            int[][] result = new int[2][];
            int j = 0, k = 0;
            int[] par = new int[tamanio];
            int[] impar = new int[tamanio];

            for (int i = 0; i < numeros.Length; i++)
            {
                if (numeros[i] % 2 == 0)
                {
                    par[j] = numeros[i];
                    j++;
                }
                else
                {
                    impar[k] = numeros[i];
                    k++;
                }
            }

            Array.Resize(ref par, j);
            Array.Sort(par);
            Array.Resize(ref impar, k);
            Array.Sort(impar);
            result = new int[2][] { par, impar };
            if (par[0] > impar[0])
                result = new int[2][] { impar, par };
            else
                result = new int[2][] { par, impar };
            return result;
        }
    }
}
