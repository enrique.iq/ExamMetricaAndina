﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using App.API.DataProvider;
using App.API.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.API.Controllers
{
    [Route("api/sucursal")]
    public class SucursalController : Controller
    {
        private ISucursalDAO proveedorDAO;
        public SucursalController(ISucursalDAO proveedorDAO)
        {
            this.proveedorDAO = proveedorDAO;
        }

        [HttpGet]
        public async Task<IEnumerable<Sucursal>> Get()
        {
            return await this.proveedorDAO.Get();
        }

        [HttpGet("{id}")]
        public async Task<Sucursal> Get(int id)
        {
            return await this.proveedorDAO.GetById(id);
        }

        [HttpPost]
        public async Task Post([FromBody]Sucursal obj)
        {
            await this.proveedorDAO.Add(obj);
        }

        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody]Sucursal obj)
        {
            await this.proveedorDAO.Update(obj);
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await this.proveedorDAO.Delete(id);
        }

        [HttpGet]
        [Route("banco/{bancoId}")]
        public async Task<IEnumerable<Sucursal>> GetSucursalByBanco(int bancoId)
        {
            return await this.proveedorDAO.GetSucursalByBanco(bancoId);
        }


    }
}
