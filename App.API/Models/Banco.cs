﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.API.Models
{
    public class Banco : EntityBase
    {
        public Banco() {
            Sucursal = new HashSet<Sucursal>();
        }
        public int? BancoId { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public virtual ICollection<Sucursal> Sucursal { get; set; }
    }
}
