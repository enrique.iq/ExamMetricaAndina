﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.API.Models
{
    public class Moneda
    {
        public int? MonedaId { get; set; }
        public string Nombre { get; set; }
    }
}
