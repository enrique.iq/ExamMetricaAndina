﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.API.Models;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace App.API.DataProvider
{
    public class SucursalDAO : ISucursalDAO
    {
        
        IConfiguration configuration;
        //private SqlConnection sqlConnection;
        public SucursalDAO(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public async Task<int> Add(Sucursal obj)
        { 
            

            int result = 0;
            try
            {
                using (var sqlConnection = new SqlConnection(this.configuration.GetConnectionString("cnn")))
                {
                    obj.CreatedBy = "einca";
                    await sqlConnection.OpenAsync().ConfigureAwait(false);
                    using (var cmd = sqlConnection.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "AGREGAR_SUCURSAL";
                        cmd.Parameters.AddWithValue("@BancoId", obj.BancoId);
                        cmd.Parameters.AddWithValue("@Nombre", obj.Nombre);
                        cmd.Parameters.AddWithValue("@Direccion", obj.Direccion);
                        cmd.Parameters.AddWithValue("@Usuario", obj.CreatedBy);
                        await cmd.ExecuteNonQueryAsync().ConfigureAwait(false);
                        result = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }

        public async Task<int> Delete(int id)
        {
            int result = 0;
            try
            {
                using (var sqlConnection = new SqlConnection(this.configuration.GetConnectionString("cnn")))
                {
                    await sqlConnection.OpenAsync().ConfigureAwait(false);
                    using (var cmd = sqlConnection.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "ELIMINAR_SUCURSAL";
                        cmd.Parameters.AddWithValue("@SucursalId", id);
                        await cmd.ExecuteNonQueryAsync().ConfigureAwait(false);
                        result = 1;
                    }
                }
            }
            catch (Exception)
            {
                result = 0;
            }
            return result;
        }

        public async Task<IEnumerable<Sucursal>> Get()
        {
            List<Sucursal> lst = new List<Sucursal>();
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(this.configuration.GetConnectionString("cnn")))
                {
                    await sqlConnection.OpenAsync().ConfigureAwait(false);
                    using (var cmd = sqlConnection.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "LISTAR_SUCURSAL";
                        var _reader = await cmd.ExecuteReaderAsync().ConfigureAwait(false);
                        while (await _reader.ReadAsync().ConfigureAwait(false))
                        {
                            lst.Add(new Sucursal
                            {
                                SucursalId = Convert.ToInt32(_reader["SucursalId"]),
                                BancoId = Convert.ToInt32(_reader["BancoId"]),
                                Nombre = Convert.ToString(_reader["Sucursal"]),
                                Direccion = Convert.ToString(_reader["Direccion"]),
                                CreatedBy = Convert.ToString(_reader["CreatedBy"]),
                                CreatedOn = Convert.ToDateTime(_reader["CreatedOn"]),
                                Banco = new Banco { BancoId = Convert.ToInt32(_reader["BancoId"]), Nombre = Convert.ToString(_reader["Banco"]) }
                            });
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                lst = null;
            }
            return lst.ToList();
        }

        public async Task<Sucursal> GetById(int id)
        {
            var obj = new Sucursal();
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(this.configuration.GetConnectionString("cnn")))
                {
                    await sqlConnection.OpenAsync().ConfigureAwait(false);
                    using (var cmd = sqlConnection.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "OBTENER_SUCURSAL";
                        cmd.Parameters.AddWithValue("@SucursalId", id);
                        var _reader = await cmd.ExecuteReaderAsync().ConfigureAwait(false);
                        while (await _reader.ReadAsync().ConfigureAwait(false))
                        {
                            obj = new Sucursal
                            {
                                SucursalId = Convert.ToInt32(_reader["SucursalId"]),
                                BancoId = Convert.ToInt32(_reader["BancoId"]),
                                Nombre = Convert.ToString(_reader["Sucursal"]),
                                Direccion = Convert.ToString(_reader["Direccion"]),
                                CreatedBy = Convert.ToString(_reader["CreatedBy"]),
                                CreatedOn = Convert.ToDateTime(_reader["CreatedOn"]),
                                Banco = new Banco { BancoId = Convert.ToInt32(_reader["BancoId"]), Nombre = Convert.ToString(_reader["Banco"]) }
                            };
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<IEnumerable<Sucursal>> GetSucursalByBanco(int bancoId)
        {
            List<Sucursal> lst = new List<Sucursal>();
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(this.configuration.GetConnectionString("cnn")))
                {
                    await sqlConnection.OpenAsync().ConfigureAwait(false);
                    using (var cmd = sqlConnection.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "LISTAR_SUCURSAL_POR_BANCO";
                        cmd.Parameters.AddWithValue("@BancoId", bancoId);
                        var _reader = await cmd.ExecuteReaderAsync().ConfigureAwait(false);
                        while (await _reader.ReadAsync().ConfigureAwait(false))
                        {
                            lst.Add(new Sucursal
                            {
                                SucursalId = Convert.ToInt32(_reader["SucursalId"]),
                                BancoId = Convert.ToInt32(_reader["BancoId"]),
                                Nombre = Convert.ToString(_reader["Sucursal"]),
                                Direccion = Convert.ToString(_reader["Direccion"]),
                                CreatedBy = Convert.ToString(_reader["CreatedBy"]),
                                CreatedOn = Convert.ToDateTime(_reader["CreatedOn"]),
                                Banco = new Banco { BancoId = Convert.ToInt32(_reader["BancoId"]), Nombre = Convert.ToString(_reader["Banco"]) }
                            });
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                lst = null;
            }
            return lst.ToList();
        }

        public async Task<int> Update(Sucursal obj)
        {
            int result = 0;
            try
            {
                using (var sqlConnection = new SqlConnection(this.configuration.GetConnectionString("cnn")))
                {
                    await sqlConnection.OpenAsync().ConfigureAwait(false);
                    using (var cmd = sqlConnection.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "ACTUALIZAR_SUCURSAL";
                        cmd.Parameters.AddWithValue("@BancoId", obj.BancoId);
                        cmd.Parameters.AddWithValue("@SucursalId", obj.SucursalId);
                        cmd.Parameters.AddWithValue("@Nombre", obj.Nombre);
                        cmd.Parameters.AddWithValue("@Direccion", obj.Direccion);
                        cmd.Parameters.AddWithValue("@Usuario", obj.CreatedBy);
                        await cmd.ExecuteNonQueryAsync().ConfigureAwait(false);
                        result = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }
    }
}
