﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.API.Models;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Extensions.Configuration;

namespace App.API.DataProvider
{
    public class OrdenPagoDAO : IOrdenPagoDAO
    {
      
        IConfiguration configuration;
        //private SqlConnection sqlConnection;
        public OrdenPagoDAO(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public async Task<int> Add(OrdenPago obj)
        {
            int result = 0;
            try
            {
                using (var sqlConnection = new SqlConnection(this.configuration.GetConnectionString("cnn")))
                {

                    obj.CreatedBy = "einca";
                    await sqlConnection.OpenAsync().ConfigureAwait(false);
                    using (var cmd = sqlConnection.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "AGREGAR_ORDENPAGO";
                        cmd.Parameters.AddWithValue("@SucursalId", obj.SucursalId);
                        cmd.Parameters.AddWithValue("@MonedaId", obj.MonedaId);
                        cmd.Parameters.AddWithValue("@EstadoOrdenPagoId", obj.EstadoOrdenPagoId);
                        cmd.Parameters.AddWithValue("@Monto", obj.Monto.HasValue ? Convert.ToDecimal(obj.Monto) : SqlDecimal.Null);
                        cmd.Parameters.AddWithValue("@FechaPago", obj.FechaPago.HasValue ? Convert.ToDateTime(obj.FechaPago) : SqlDateTime.Null);
                        cmd.Parameters.AddWithValue("@Usuario", obj.CreatedBy);

                        await cmd.ExecuteNonQueryAsync().ConfigureAwait(false);
                        result = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }

        public async Task<int> Delete(int id)
        {
            int result = 0;
            try
            {
                using (var sqlConnection = new SqlConnection(this.configuration.GetConnectionString("cnn")))
                {
                    await sqlConnection.OpenAsync().ConfigureAwait(false);
                    using (var cmd = sqlConnection.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "ELIMINAR_ORDENPAGO";
                        cmd.Parameters.AddWithValue("@OrdenPagoId", id);
                        await cmd.ExecuteNonQueryAsync().ConfigureAwait(false);
                        result = 1;
                    }
                }
            }
            catch (Exception)
            {
                result = 0;
            }
            return result;
        }

        public async Task<IEnumerable<OrdenPago>> Get()
        {
            List<OrdenPago> lst = new List<OrdenPago>();
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(this.configuration.GetConnectionString("cnn")))
                {
                    await sqlConnection.OpenAsync().ConfigureAwait(false);
                    using (var cmd = sqlConnection.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "LISTAR_ORDENPAGO";
                        var _reader = await cmd.ExecuteReaderAsync().ConfigureAwait(false);
                        while (await _reader.ReadAsync().ConfigureAwait(false))
                        {
                            lst.Add(new OrdenPago
                            {
                                OrdenPagoId = Convert.ToInt32(_reader["OrdenPagoId"]),
                                FechaPago = (_reader.IsDBNull(_reader.GetOrdinal("FechaPago"))) ? (DateTime?)null : Convert.ToDateTime(_reader["FechaPago"]),
                                Monto = Convert.ToDecimal(_reader["Monto"]),
                                Moneda = new Moneda
                                {
                                    MonedaId = Convert.ToInt32(_reader["MonedaId"]),
                                    Nombre = Convert.ToString(_reader["Moneda"])
                                },
                                EstadoOrdenPago = new EstadoOrdenPago
                                {
                                    Nombre = Convert.ToString(_reader["EstadoOrdenPago"]),
                                    EstadoOrdenPagoId = Convert.ToInt32(_reader["EstadoOrdenPagoId"])
                                },
                                Sucursal = new Sucursal
                                {
                                    SucursalId = Convert.ToInt32(_reader["SucursalId"]),
                                    BancoId = Convert.ToInt32(_reader["BancoId"]),
                                    Nombre = Convert.ToString(_reader["Sucursal"]),
                                    Direccion = Convert.ToString(_reader["DireccionSucursal"]),
                                    Banco = new Banco
                                    {
                                        BancoId = Convert.ToInt32(_reader["BancoId"]),
                                        Nombre = Convert.ToString(_reader["Banco"])
                                    }
                                },
                                CreatedBy = Convert.ToString(_reader["CreatedBy"]),
                                CreatedOn = Convert.ToDateTime(_reader["CreatedOn"])
                            });
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                lst = null;
            }
            return lst.ToList();
        }

        public async Task<OrdenPago> GetById(int id)
        {
            var obj = new OrdenPago();
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(this.configuration.GetConnectionString("cnn")))
                {
                    await sqlConnection.OpenAsync().ConfigureAwait(false);
                    using (var cmd = sqlConnection.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "OBTENER_ORDENPAGO";
                        cmd.Parameters.AddWithValue("@OrdePagoId", id);
                        var _reader = await cmd.ExecuteReaderAsync().ConfigureAwait(false);
                        while (await _reader.ReadAsync().ConfigureAwait(false))
                        {
                            obj = new OrdenPago
                            {
                                OrdenPagoId = Convert.ToInt32(_reader["OrdenPagoId"]),
                                MonedaId = Convert.ToInt32(_reader["MonedaId"]),
                                EstadoOrdenPagoId = Convert.ToInt32(_reader["EstadoOrdenPagoId"]),
                                SucursalId = Convert.ToInt32(_reader["SucursalId"]),
                                FechaPago = (_reader.IsDBNull(_reader.GetOrdinal("FechaPago"))) ? (DateTime?)null : Convert.ToDateTime(_reader["FechaPago"]),
                                Monto = Convert.ToDecimal(_reader["Monto"]),
                                Moneda = new Moneda
                                {
                                    MonedaId = Convert.ToInt32(_reader["MonedaId"]),
                                    Nombre = Convert.ToString(_reader["Moneda"])
                                },
                                EstadoOrdenPago = new EstadoOrdenPago
                                {
                                    Nombre = Convert.ToString(_reader["EstadoOrdenPago"]),
                                    EstadoOrdenPagoId = Convert.ToInt32(_reader["EstadoOrdenPagoId"])
                                },
                                Sucursal = new Sucursal
                                {
                                    SucursalId = Convert.ToInt32(_reader["SucursalId"]),
                                    BancoId = Convert.ToInt32(_reader["BancoId"]),
                                    Nombre = Convert.ToString(_reader["Sucursal"]),
                                    Direccion = Convert.ToString(_reader["DireccionSucursal"]),
                                    Banco = new Banco
                                    {
                                        BancoId = Convert.ToInt32(_reader["BancoId"]),
                                        Nombre = Convert.ToString(_reader["Banco"])
                                    }
                                },
                                CreatedBy = Convert.ToString(_reader["CreatedBy"]),
                                CreatedOn = Convert.ToDateTime(_reader["CreatedOn"])
                            };
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

       
        public async Task<IEnumerable<OrdenPago>> GetOrdenPagoBySucursal(int SucursalId, string monedaId)
        {
            List<OrdenPago> lst = new List<OrdenPago>();
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(this.configuration.GetConnectionString("cnn")))
                {
                    await sqlConnection.OpenAsync().ConfigureAwait(false);
                    using (var cmd = sqlConnection.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "LISTAR_ORDENPAGO_POR_SUCURSAL";
                        cmd.Parameters.AddWithValue("@SucursalId", SucursalId);
                        cmd.Parameters.AddWithValue("@MonedaId", monedaId);
                        var _reader = await cmd.ExecuteReaderAsync().ConfigureAwait(false);
                        while (await _reader.ReadAsync().ConfigureAwait(false))
                        {
                            lst.Add(new OrdenPago
                            {
                                OrdenPagoId = Convert.ToInt32(_reader["OrdenPagoId"]),
                                FechaPago = (_reader.IsDBNull(_reader.GetOrdinal("FechaPago"))) ? (DateTime?)null : Convert.ToDateTime(_reader["FechaPago"]),
                                Monto = Convert.ToDecimal(_reader["Monto"]),
                                MonedaId = Convert.ToInt32(_reader["MonedaId"]),
                                Moneda = new Moneda
                                {
                                    MonedaId = Convert.ToInt32(_reader["MonedaId"]),
                                    Nombre = Convert.ToString(_reader["Moneda"])
                                },
                                EstadoOrdenPago = new EstadoOrdenPago
                                {
                                    Nombre = Convert.ToString(_reader["EstadoOrdenPago"]),
                                    EstadoOrdenPagoId = Convert.ToInt32(_reader["EstadoOrdenPagoId"])
                                },
                                Sucursal = new Sucursal
                                {
                                    SucursalId = Convert.ToInt32(_reader["SucursalId"]),
                                    BancoId = Convert.ToInt32(_reader["BancoId"]),
                                    Nombre = Convert.ToString(_reader["Sucursal"]),
                                    Direccion = Convert.ToString(_reader["DireccionSucursal"]),
                                    Banco = new Banco
                                    {
                                        BancoId = Convert.ToInt32(_reader["BancoId"]),
                                        Nombre = Convert.ToString(_reader["Banco"])
                                    }
                                },
                                CreatedBy = Convert.ToString(_reader["CreatedBy"]),
                                CreatedOn = Convert.ToDateTime(_reader["CreatedOn"])
                            });
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                lst = null;
            }
            return lst.ToList();
        }

        public async Task<int> Update(OrdenPago obj)
        {
            int result = 0;
            try
            {
                using (var sqlConnection = new SqlConnection(this.configuration.GetConnectionString("cnn")))
                {
                    await sqlConnection.OpenAsync().ConfigureAwait(false);
                    using (var cmd = sqlConnection.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "ACTUALIZAR_ORDENPAGO";
                        cmd.Parameters.AddWithValue("@OrdenPagoId", obj.OrdenPagoId);
                        cmd.Parameters.AddWithValue("@Monto", obj.Monto.HasValue ? Convert.ToDecimal(obj.Monto) : SqlDecimal.Null);
                        cmd.Parameters.AddWithValue("@EstadoOrdenPagoId", obj.EstadoOrdenPagoId);
                        cmd.Parameters.AddWithValue("@FechaPago", obj.FechaPago.HasValue ? Convert.ToDateTime(obj.FechaPago) : SqlDateTime.Null);
                        cmd.Parameters.AddWithValue("@Usuario", obj.CreatedBy);
                        await cmd.ExecuteNonQueryAsync().ConfigureAwait(false);
                        result = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }
    }
}
